# NGINX Amplify Agent (For Watcher Nginx Controller)

Orjinal Amplify Agent üzerinden fork alınarak oluşturulmuştur. Kurulum için aşağıdaki adımları izleyebilirsiniz. __Öneri: İlk etapta oluşturulan RPM dosyasını, kendi repositorylerinize ekleyerek buradan dağıtabilirsiniz.__

```bash
git clone https://yigitcanbasalma@bitbucket.org/yigitcanbasalma/nginx-amplify-agent.git
# Sunucu üzerinde rpm-build paketi yüklü olmalıdır. Değilse;
# yum install -y rpm-build
tools/builder.py # RPM package build
API_KEY="<api_key>" INSTALL_FROM_RPM="</path/to/rpm>" packages/install.sh
```

__ÖNEMLİ:__ Scriptin çalışabilmesi için aşaıdaki sudo tanımı yapılmalı ve /etc/nginx klasörü ve altındaki tüm dizin ve dosyaların sahipliği "nginx:nginx" olarak güncellenmelidir.
````
nginx           ALL=(ALL:ALL)   NOPASSWD: /usr/sbin/service nginx *, /usr/sbin/nginx *, /usr/sbin/service amplify-agent *
````